package dev.rovearix.mc.cratedrops.utils

import dev.rovearix.mc.cratedrops.CrateDrops
import dev.rovearix.mc.vst.sponge.utils.SpongePermissionUtils

class CratePermissionUtils(
    plugin: CrateDrops
) : SpongePermissionUtils(plugin) {
    override fun registerPermissions() {
        registerPermission(DROP_CRATE_PERMISSION)
        registerPermission(ACTIVE_CRATES_PERMISSION)
        registerPermission(CLEAR_ACTIVE_CRATES_PERMISSION)
        registerPermission(TP_CRATE_PERMISSION)
        registerPermission(TP_CRATE_OTHERS_PERMISSION)
        registerPermission(WARP_TEXT_PERMISSION)
        registerPermission(ADMIN_DEBUG_PERMISSION)
    }

    override val ADMIN_DEBUG_PERMISSION = Permission("cratedrops.admin.debug")

    companion object {
        val DROP_CRATE_PERMISSION = Permission("cratedrops.command.dropcrate")
        val ACTIVE_CRATES_PERMISSION = Permission("cratedrops.command.activecrates")
        val CLEAR_ACTIVE_CRATES_PERMISSION = Permission("cratedrops.command.clearactivecrates")
        val TP_CRATE_PERMISSION = Permission("cratedrops.command.tpcrate.self")
        val TP_CRATE_OTHERS_PERMISSION = Permission("cratedrops.command.tpcrate.others")
        val WARP_TEXT_PERMISSION = Permission("cratedrops.admin.warptext")
    }
}
