package dev.rovearix.mc.cratedrops.utils

import dev.rovearix.mc.cratedrops.CrateDrops
import dev.rovearix.mc.cratedrops.data.Crate
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.event.ClickEvent
import net.kyori.adventure.text.event.HoverEvent
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer
import org.spongepowered.api.entity.living.player.server.ServerPlayer
import org.spongepowered.api.world.server.ServerLocation
import java.util.*

class TextUtils(
    private val plugin: CrateDrops
) {
    fun generateCrateBroadcastMessage(dropLocation: ServerLocation, selectedCrate: Crate): Component {
        return parseMessage(
            if (selectedCrate.isBroadcastCustomMessage) {
                selectedCrate.customMessage
            } else {
                plugin.configUtils.broadcastMessage
            },
            dropLocation.blockX(),
            dropLocation.blockY(),
            dropLocation.blockZ(),
            selectedCrate.name
        )
    }

    fun generateCrateBroadcastExpireMessage(dropLocation: ServerLocation, crateName: String): Component {
        return parseMessage(
            plugin.configUtils.expiredCrateMessage,
            dropLocation.blockX(),
            dropLocation.blockY(),
            dropLocation.blockZ(),
            crateName
        )
    }

    fun generateCrateAutoDropMessage(time: String): Component {
        return parseMessage(plugin.configUtils.autoDropMessage, time)
    }

    fun generateCrateFoundMessage(source: ServerPlayer, foundLocation: ServerLocation, crateName: String): Component {
        return parseMessage(
            source,
            plugin.configUtils.crateFoundMessage,
            foundLocation.blockX(),
            foundLocation.blockY(),
            foundLocation.blockZ(),
            crateName
        )
    }

    fun generateTeleportMessage(message: Component, crateId: Int): Component {
        return message
            .clickEvent(ClickEvent.runCommand("/tpcrate $crateId"))
            .hoverEvent(HoverEvent.showText(Component.text("Click here to teleport to the crate!")))
    }

    private fun cratePretty(crate: String): String {
        return crate.split("-").joinToString(separator = " ") { word ->
            word.lowercase(Locale.getDefault()).replaceFirstChar { it.uppercaseChar() }
        }
    }

    private fun parseMessage(
        source: ServerPlayer,
        rawMessage: String,
        x: Int,
        y: Int,
        z: Int,
        crate: String
    ): Component {
        return parseMessage(rawMessage.replace("{PLAYER}", source.name()), x, y, z, crate)
    }

    private fun parseMessage(rawMessage: String, time: String): Component {
        return parseMessage(rawMessage.replace("{TIME}", time))
    }

    private fun parseMessage(rawMessage: String, x: Int, y: Int, z: Int, crate: String): Component {
        return parseMessage(
            rawMessage
                .replace("{X}", x.toString())
                .replace("{Y}", y.toString())
                .replace("{Z}", z.toString())
                .replace("{CRATE}", cratePretty(crate))
        )
    }

    private fun parseMessage(rawMessage: String): Component {
        return LegacyComponentSerializer.legacyAmpersand().deserialize(rawMessage)
    }
}
