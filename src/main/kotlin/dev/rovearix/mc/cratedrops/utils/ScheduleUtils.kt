package dev.rovearix.mc.cratedrops.utils

import dev.rovearix.mc.cratedrops.CrateDrops
import dev.rovearix.mc.cratedrops.tasks.CrateDropTask
import org.spongepowered.api.Sponge
import org.spongepowered.api.scheduler.ScheduledTask
import org.spongepowered.api.scheduler.Task
import java.time.Duration
import java.util.*

class ScheduleUtils(
    private val plugin: CrateDrops
) {
    private var dropTask: ScheduledTask? = null

    private val configScheduleTime: Long
        get() =
            convertTimeFormatToLong(
                plugin.configUtils.autoDropInterval.replace(" ".toRegex(), "").lowercase(Locale.getDefault())
            )

    fun scheduleDrop() {
        resetDropTimer()
        if (plugin.configUtils.isAutoDropEnabled) {
            val time = configScheduleTime
            if (time > 0) {
                dropTask = Sponge.server().scheduler().submit(
                    Task.builder()
                        .plugin(plugin.container)
                        .delay(Duration.ofMillis(time))
                        .execute(CrateDropTask(plugin))
                        .build(),
                    DROP_TASK_NAME
                )
                plugin.messageUtils.announceCreateAutoDrop(convertTime(time))
            } else {
                plugin.messageUtils.notifyError("Invalid time in config, disabling auto drop.")
            }
        }
    }

    private fun resetDropTimer() {
        dropTask?.cancel()
    }

    private fun convertTime(time: Long): String {
        var remainingTime = time
        var timeReal = ""
        timeReal += getRemainderString(ONE_DAY, "Day", remainingTime)
        remainingTime %= ONE_DAY
        timeReal += " " + getRemainderString(ONE_HOUR, "Hour", remainingTime)
        remainingTime %= ONE_HOUR
        timeReal += " " + getRemainderString(ONE_MINUTE, "Minute", remainingTime)
        remainingTime %= ONE_MINUTE
        timeReal += " " + getRemainderString(ONE_SECOND, "Second", remainingTime)
        return timeReal.trim()
    }

    private fun getRemainderString(unitOfTime: Long, unitName: String, time: Long): String {
        var remainder = ""
        val units = time / unitOfTime
        if (units > 0) {
            remainder = "$units $unitName"
            if (units != 1L) {
                remainder += "s"
            }
        }
        return remainder
    }

    private fun convertTimeFormatToLong(timeRaw: String): Long {
        var remainingTimeRaw = timeRaw
        var time = 0L
        time += pullUnitsFromString("d", ONE_DAY, remainingTimeRaw)
        remainingTimeRaw = removeUnitsFromString("d", remainingTimeRaw)
        time += pullUnitsFromString("h", ONE_HOUR, remainingTimeRaw)
        remainingTimeRaw = removeUnitsFromString("h", remainingTimeRaw)
        time += pullUnitsFromString("m", ONE_MINUTE, remainingTimeRaw)
        remainingTimeRaw = removeUnitsFromString("m", remainingTimeRaw)
        time += pullUnitsFromString("s", ONE_SECOND, remainingTimeRaw)
        remainingTimeRaw = removeUnitsFromString("s", remainingTimeRaw)
        if (remainingTimeRaw.isNotEmpty()) {
            return -1
        }
        return time
    }

    private fun pullUnitsFromString(unitMarker: String, unitWeight: Long, timeRaw: String): Long {
        val timeSplit = timeRaw.split(unitMarker.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (timeSplit.size == 1 && (timeSplit[0].isEmpty() || timeSplit[0] == timeRaw)) {
            return 0
        }
        return timeSplit[0].toLong() * unitWeight
    }

    private fun removeUnitsFromString(unitMarker: String, timeRaw: String): String {
        val timeSplit = timeRaw.split(unitMarker.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (isLastCharUnitMarker(unitMarker, timeRaw)) {
            return ""
        } else if (timeSplit.size == 1) {
            return timeRaw
        }
        return timeSplit[1]
    }

    private fun isLastCharUnitMarker(unitMarker: String, timeRaw: String): Boolean {
        return timeRaw.isNotEmpty() && timeRaw[timeRaw.length - 1] == unitMarker[0]
    }

    companion object {
        private const val DROP_TASK_NAME = "cratedrops.scheduleddrop"

        private const val ONE_SECOND: Long = 1000
        private const val ONE_MINUTE: Long = ONE_SECOND * 60
        private const val ONE_HOUR: Long = ONE_MINUTE * 60
        private const val ONE_DAY: Long = ONE_HOUR * 24
    }
}
