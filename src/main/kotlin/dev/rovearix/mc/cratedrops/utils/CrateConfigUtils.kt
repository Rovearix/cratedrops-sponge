package dev.rovearix.mc.cratedrops.utils

import dev.rovearix.mc.cratedrops.CrateDrops
import dev.rovearix.mc.vst.shared.data.Zone
import dev.rovearix.mc.vst.sponge.utils.SpongeConfigUtils
import org.spongepowered.api.ResourceKey
import org.spongepowered.api.Sponge
import org.spongepowered.api.world.server.ServerWorld
import org.spongepowered.configurate.ConfigurationNode

class CrateConfigUtils(plugin: CrateDrops) : SpongeConfigUtils(plugin) {
    override val configName = "cratedrops.conf"
    override val assetsPath: String = "assets/cratedrops"
    override val configVersion = 1.1

    val allCrateNodes: Collection<ConfigurationNode>
        get() = config!!.node("config", "crates").childrenMap().values

    val allKitNodes: Collection<ConfigurationNode>
        get() = config!!.node("config", "kits").childrenMap().values

    val spawnRadius: Int
        get() = getIntFromConfig("spawn-radius")

    private val worldName: String
        get() = getStringFromConfig("world-name")

    val selectedWorld: ServerWorld?
        get() {
            val worldParts = worldName.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (worldParts.size == 2) {
                val namespace = worldParts[0]
                val name = worldParts[1]
                return Sponge.server().worldManager().world(ResourceKey.of(namespace, name)).orElse(null)
            }
            return null
        }

    val isDropRestrictionSet: Boolean
        get() = getBooleanFromConfig("restrict-drop", "enabled")

    val dropRestrictionFromConfig: Zone
        get() = getZoneFromConfig("restrict-drop")

    val broadcastMessage: String
        get() = getStringFromConfig("broadcast-message")

    val isBroadcastExpireSet: Boolean
        get() = getBooleanFromConfig("buffer", "broadcast-expire")

    val expiredCrateMessage: String
        get() = getStringFromConfig("buffer", "expire-message")

    val isAutoDropEnabled: Boolean
        get() = getBooleanFromConfig("auto-drop", "enabled")

    val autoDropInterval: String
        get() = getStringFromConfig("auto-drop", "interval")

    val isAutoDropRandomKitEnabled: Boolean
        get() = getBooleanFromConfig("auto-drop", "random-kit")

    val isAutoDropBroadcastEnabled: Boolean
        get() = getBooleanFromConfig("auto-drop", "broadcast-auto")

    val autoDropMessage: String
        get() = getStringFromConfig("auto-drop", "auto-message")

    val autoDropDefinedKit: String
        get() = getStringFromConfig("auto-drop", "set-kit")

    val crateFoundMessage: String
        get() = getStringFromConfig("found-message")

    fun getCrateKitMessage(crate: String): String {
        return getStringFromConfig("crates", crate, "kit-name")
    }
}
