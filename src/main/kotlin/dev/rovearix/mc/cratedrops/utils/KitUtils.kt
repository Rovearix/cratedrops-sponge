package dev.rovearix.mc.cratedrops.utils

import dev.rovearix.mc.cratedrops.CrateDrops
import dev.rovearix.mc.cratedrops.data.Kit
import org.spongepowered.api.data.Keys
import org.spongepowered.api.entity.Entity
import org.spongepowered.api.entity.EntityTypes
import org.spongepowered.api.world.server.ServerLocation

class KitUtils(
    private val plugin: CrateDrops
) {
    private var registeredKits: List<Kit> = listOf()

    fun registerKitsFromConfig() {
        val kitNodes = plugin.configUtils.allKitNodes
        if (kitNodes.isEmpty()) {
            plugin.messageUtils.notifyError("No kits defined in the config!")
        }

        registeredKits = kitNodes.mapNotNull { kitNode ->
            Kit(kitNode).let { kit ->
                if (kit.isValid) {
                    kit
                } else {
                    kit.errorMessages.forEach { plugin.messageUtils.notifyError(it) }
                    null
                }
            }
        }
    }

    fun redeemCrateKit(kitName: String, crateLocation: ServerLocation): Boolean {
        val setCrateKit = registeredKits.firstOrNull { it.name == kitName }
        if (setCrateKit == null) {
            plugin.messageUtils.notifyError("Requested kit $kitName does not exist!")
            return false
        }
        for (item in setCrateKit.rewards) {
            val itemEntity: Entity = crateLocation.world().createEntity(EntityTypes.ITEM, crateLocation.position())
            itemEntity.offer(Keys.ITEM_STACK_SNAPSHOT, item.createSnapshot())
            if (!crateLocation.world().spawnEntity(itemEntity)) {
                return false
            }
        }
        return true
    }

    fun getRegisteredKits(): List<Kit> {
        return registeredKits
    }
}
