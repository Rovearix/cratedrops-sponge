package dev.rovearix.mc.cratedrops.utils

import dev.rovearix.mc.cratedrops.CrateDrops
import dev.rovearix.mc.cratedrops.data.Crate
import dev.rovearix.mc.vst.sponge.utils.SpongeMessageUtils
import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer
import org.apache.logging.log4j.Logger
import org.spongepowered.api.entity.living.player.server.ServerPlayer
import org.spongepowered.api.world.server.ServerLocation

class CrateMessageUtils(
    private val plugin: CrateDrops,
    logger: Logger
) : SpongeMessageUtils(plugin, logger) {
    fun announceCrate(dropLocation: ServerLocation, selectedCrate: Crate) {
        val message = plugin.textUtils.generateCrateBroadcastMessage(dropLocation, selectedCrate)
        val teleportMessage = plugin.textUtils.generateTeleportMessage(
            message,
            plugin.crateDatabase.allCrateLocations.indexOf(dropLocation)
        )
        broadcastMessage(
            teleportMessage,
            permissionNode = CratePermissionUtils.WARP_TEXT_PERMISSION,
            noPermissionMessage = message
        )
    }

    fun announceCrateExpired(dropLocation: ServerLocation, crateName: String) {
        val message = plugin.textUtils.generateCrateBroadcastExpireMessage(dropLocation, crateName)
        if (plugin.configUtils.isBroadcastExpireSet) {
            broadcastMessage(message)
        } else {
            logger.info(PlainTextComponentSerializer.plainText().serialize(message))
        }
    }

    fun announceCreateAutoDrop(time: String) {
        val message = plugin.textUtils.generateCrateAutoDropMessage(time)
        if (plugin.configUtils.isAutoDropBroadcastEnabled) {
            broadcastMessage(message)
        } else {
            logger.info(PlainTextComponentSerializer.plainText().serialize(message))
        }
    }

    fun announceCrateFound(source: ServerPlayer, foundLocation: ServerLocation, crateName: String) {
        val message = plugin.textUtils.generateCrateFoundMessage(source, foundLocation, crateName)
        broadcastMessage(message)
    }
}
