package dev.rovearix.mc.cratedrops.data

import org.spongepowered.api.ResourceKey
import org.spongepowered.api.Sponge
import org.spongepowered.api.item.inventory.ItemStack
import org.spongepowered.api.registry.RegistryKey
import org.spongepowered.api.registry.RegistryTypes
import org.spongepowered.configurate.ConfigurationNode

class Kit(
    kitNode: ConfigurationNode,
) : ConfigSourced(kitNode.key() as String) {
    val rewards: List<ItemStack> = convertRewardsToItems(kitNode.node("rewards"))

    private fun convertRewardsToItems(rewardsNode: ConfigurationNode): List<ItemStack> {
        return rewardsNode.childrenMap().map { (id, itemConfig) ->
            val itemId = id as String
            val quantity = itemConfig.int
            //TODO: Add tests
            ItemStack.builder()
                .itemType(
                    RegistryKey.of(
                        RegistryTypes.ITEM_TYPE,
                        ResourceKey.resolve(itemId)
                    ).asDefaultedReference { Sponge.game() }
                )
                .quantity(quantity)
                .build()
        }
    }
}