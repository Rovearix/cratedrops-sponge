package dev.rovearix.mc.cratedrops.data

import dev.rovearix.mc.cratedrops.CrateDrops
import dev.rovearix.mc.cratedrops.commands.DropCrateCommand
import dev.rovearix.mc.vst.shared.data.Database
import org.spongepowered.api.world.server.ServerLocation

class CrateDatabase(
    private val plugin: CrateDrops
) : Database<ServerLocation, String>(plugin) {
    // TODO: Replace database with indexed crates so we can call them after the fact.

    override val databaseName = "offline.db"

    val allCrateLocations: List<ServerLocation>
        get() = database.keys.toList()

    override fun convertRawToKey(rawCrate: String): ServerLocation {
        val crateData = rawCrate.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val x = crateData[0].toInt()
        val y = crateData[1].toInt()
        val z = crateData[2].toInt()

        val crateLoc = plugin.cache.targetWorld.location(x, y, z)
        crateLoc.setBlockType(DropCrateCommand.Companion.CRATE_BLOCK_TYPE)
        return crateLoc
    }

    override fun convertRawToValue(rawCrate: String): String {
        val crateData = rawCrate.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val crate = crateData[3]
        return crate
    }

    override fun convertKeyToRaw(crateLocation: ServerLocation): String {
        val x = crateLocation.blockX()
        val y = crateLocation.blockY()
        val z = crateLocation.blockZ()
        return "$x,$y,$z"
    }

    override fun convertValueToRaw(crateName: String): String {
        return crateName
    }

    override fun doesRawMatchKey(rawCrate: String, location: ServerLocation): Boolean {
        val crateData = rawCrate.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (crateData[0].toInt() != location.blockX()) {
            return false
        } else if (crateData[1].toInt() != location.blockY()) {
            return false
        }
        return crateData[2].toInt() == location.blockZ()
    }

    fun isCrateAtLocation(location: ServerLocation): Boolean {
        return tryGetValue(location) != null
    }

    fun getCrateAtLocation(location: ServerLocation): String? {
        return tryGetValue(location)
    }

    fun addCrate(location: ServerLocation, selectedCrate: Crate) {
        addEntry(location, selectedCrate.name)
    }

    fun removeCrate(crateLocation: ServerLocation) {
        removeCrateFromOfflineDB(crateLocation)
    }

    fun removeOldestCrate() {
        removeOldestEntry { location, name ->
            location.removeBlock()
            plugin.messageUtils.announceCrateExpired(location, name)
        }
    }
}