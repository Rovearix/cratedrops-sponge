package dev.rovearix.mc.cratedrops.data

import org.spongepowered.configurate.ConfigurationNode

abstract class ConfigSourced(
    val name: String
) {
    val errorMessages: List<String> = mutableListOf()

    val isValid: Boolean
        get() = errorMessages.isEmpty()

    protected fun getVariableFromNode(crateNode: ConfigurationNode, variableName: String): ConfigurationNode {
        val node = crateNode.node(variableName)
        if (node.empty()) {
            val className = this.javaClass.asSubclass(this.javaClass).name
            errorMessages.toMutableList() += "Disabling $className: $name. Reason: missing $variableName or blank!"
        }
        return node
    }
}
