package dev.rovearix.mc.cratedrops.data

import dev.rovearix.mc.cratedrops.CrateDrops
import dev.rovearix.mc.vst.shared.data.CalculationCache
import dev.rovearix.mc.vst.shared.data.Region
import dev.rovearix.mc.vst.shared.data.Zone
import org.spongepowered.api.world.server.ServerWorld
import kotlin.math.floor

class CrateCalculationCache(
    private val plugin: CrateDrops
) : CalculationCache() {
    lateinit var targetWorld: ServerWorld

    var validCrates: List<Crate> = listOf()
        private set

    var chanceTotal: Int = 0
        private set

    var dropRegion: Region = Region(Zone.fromRadius(0, 0, 10))
        private set

    override fun collectConfigInformation(): Boolean {
        plugin.configUtils.selectedWorld.let { selectedWorld ->
            if (selectedWorld == null) {
                plugin.messageUtils.notifyError("Invalid world selected in the config.")
                return false
            }
            targetWorld = selectedWorld
        }

        if (!setupInitialDropRegion()) {
            plugin.messageUtils.notifyError("Invalid parameters for drop restriction or not within world boarder.")
            return false
        }
        applyRestrictionsToDropRegion()
        return validateAndCacheCrateConfigs()
    }

    private fun setupInitialDropRegion(): Boolean {
        val worldBoarderZone = worldBoarderZone
        dropRegion = if (plugin.configUtils.isDropRestrictionSet) {
            val dropRestriction = plugin.configUtils.dropRestrictionFromConfig
            if (dropRestriction.isValid && worldBoarderZone.isIntersectingZone(dropRestriction)) {
                Region(worldBoarderZone, dropRestriction)
            } else {
                return false
            }
        } else {
            Region(worldBoarderZone)
        }
        return true
    }

    private fun applyRestrictionsToDropRegion() {
        dropRegion.subtractIllegalZone(spawnProtectionZone)
        if (dropRegion.legalZones.isEmpty()) {
            plugin.messageUtils.notifyError("Invalid spawn protection. Region is larger than world border.")
        }
    }

    private val worldBoarderZone: Zone
        get() {
            val border = targetWorld.border()
            val borderRadius = floor(border.diameter() / 2).toInt()
            val centerX = border.center().floorX()
            val centerZ = border.center().floorY()
            return Zone.fromRadius(centerX, centerZ, borderRadius)
        }

    private val spawnProtectionZone: Zone
        get() {
            val spawnX = targetWorld.properties().spawnPosition().x()
            val spawnZ = targetWorld.properties().spawnPosition().z()
            val spawnProtectionRadius = plugin.configUtils.spawnRadius
            return Zone.fromRadius(spawnX, spawnZ, spawnProtectionRadius)
        }

    private fun validateAndCacheCrateConfigs(): Boolean {
        val crateNodes = plugin.configUtils.allCrateNodes
        if (crateNodes.isEmpty()) {
            plugin.messageUtils.notifyError("No crates defined in the config!")
            return false
        }

        validCrates = crateNodes.mapNotNull { crateNode ->
            val crate = Crate(crateNode, plugin.kitUtils.getRegisteredKits())
            if (crate.isValid) {
                chanceTotal += crate.chance
                crate
            } else {
                crate.errorMessages.forEach { plugin.messageUtils.notifyError(it) }
                null
            }
        }

        if (validCrates.isEmpty()) {
            plugin.messageUtils.notifyError("No valid crates in the config!")
            return false
        }

        //Stops there from being only empty crates
        if (chanceTotal == 0) {
            plugin.messageUtils.notifyError("Crates all have a chance of zero percent. Update the config!")
            return false
        }
        return true
    }
}
