package dev.rovearix.mc.cratedrops.data

import dev.rovearix.mc.vst.shared.utils.RandomUtils.WeightedElement
import org.spongepowered.configurate.ConfigurationNode

class Crate(
    crateNode: ConfigurationNode,
    kitRepo: List<Kit>
) : ConfigSourced(crateNode.key() as String), WeightedElement {
    private val kitName: String = getVariableFromNode(crateNode, "kit-name").string ?: ""

    val customMessage: String = getVariableFromNode(crateNode, "custom-message").string ?: ""
    val chance: Int = getVariableFromNode(crateNode, "chance").int
    val isBroadcastCustomMessage: Boolean = getVariableFromNode(crateNode, "broadcast-custom").boolean

    init {
        if (isValid) {
            if (kitRepo.any { it.name == kitName }) {
                errorMessages.toMutableList() += "Disabling Crate: $name. Reason: Kit $kitName is not available from config!"
            }
        }
    }

    override val weight: Int
        get() = chance
}
