package dev.rovearix.mc.cratedrops

import com.google.inject.Inject
import dev.rovearix.mc.cratedrops.commands.ActiveCratesCommand
import dev.rovearix.mc.cratedrops.commands.ClearActiveCratesCommand
import dev.rovearix.mc.cratedrops.commands.DropCrateCommand
import dev.rovearix.mc.cratedrops.commands.TeleportCrateCommand
import dev.rovearix.mc.cratedrops.data.CrateCalculationCache
import dev.rovearix.mc.cratedrops.data.CrateDatabase
import dev.rovearix.mc.cratedrops.listeners.CrateOpenListener
import dev.rovearix.mc.cratedrops.utils.*
import dev.rovearix.mc.vst.shared.entity.VoidServer
import dev.rovearix.mc.vst.sponge.SpongeVoidTool
import dev.rovearix.mc.vst.sponge.commands.SpongeVoidCommand
import dev.rovearix.mc.vst.sponge.entity.SpongeVoidServer
import org.apache.logging.log4j.Logger
import org.spongepowered.api.Sponge
import org.spongepowered.api.config.ConfigDir
import org.spongepowered.api.event.Listener
import org.spongepowered.api.event.lifecycle.ConstructPluginEvent
import org.spongepowered.plugin.PluginContainer
import org.spongepowered.plugin.builtin.jvm.Plugin
import java.nio.file.Path

@Plugin("cratedrops")
class CrateDrops @Inject internal constructor(
    override val container: PluginContainer,
    @ConfigDir(sharedRoot = false)
    override val configDir: Path,
    logger: Logger
): SpongeVoidTool() {
    override val toolName: String = "CrateDrops"

    override val configUtils by lazy {
        CrateConfigUtils(this)
    }
    override val messageUtils by lazy {
        CrateMessageUtils(this, logger)
    }
    override val permissionUtils by lazy {
        CratePermissionUtils(this)
    }
    override val cache = CrateCalculationCache(this)

    override val server: VoidServer = SpongeVoidServer

    override val commands: List<SpongeVoidCommand> by lazy {
        listOf(
            DropCrateCommand(this),
            ActiveCratesCommand(this),
            ClearActiveCratesCommand(this),
            TeleportCrateCommand(this)
        )
    }

    override val additionalListeners: List<Any> = listOf(
        CrateOpenListener(this)
    )

    val crateDatabase = CrateDatabase(this)
    val kitUtils = KitUtils(this)
    val scheduleUtils = ScheduleUtils(this)
    val textUtils = TextUtils(this)

    @Listener
    fun onConstructPlugin(event: ConstructPluginEvent) {
        messageUtils.notifyConsole("Starting up CrateDrops.")
        crateDatabase.initializeDBFile(configDir)
        Sponge.eventManager().registerListeners(container, listenerWrapper)
    }

    override fun processData() {
        kitUtils.registerKitsFromConfig()
        crateDatabase.refreshDatabase()
        scheduleUtils.scheduleDrop()
    }
}
