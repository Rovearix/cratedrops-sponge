package dev.rovearix.mc.cratedrops.listeners

import dev.rovearix.mc.cratedrops.CrateDrops
import org.spongepowered.api.command.exception.CommandException
import org.spongepowered.api.data.Keys
import org.spongepowered.api.entity.Entity
import org.spongepowered.api.entity.EntityTypes
import org.spongepowered.api.entity.living.player.server.ServerPlayer
import org.spongepowered.api.event.Listener
import org.spongepowered.api.event.block.InteractBlockEvent
import org.spongepowered.api.item.FireworkEffect
import org.spongepowered.api.item.FireworkShapes
import org.spongepowered.api.util.Color
import org.spongepowered.api.util.Ticks
import org.spongepowered.api.world.server.ServerLocation
import kotlin.jvm.optionals.getOrNull

class CrateOpenListener(
    private val plugin: CrateDrops
) {
    @Listener
    fun onInteractBlockEvent(event: InteractBlockEvent) {
        event.block().location().getOrNull()?.let { interactedLocation ->
            (event.source() as? ServerPlayer)?.let { player ->
                if (plugin.crateDatabase.isCrateAtLocation(interactedLocation)) {
                    handleCrateAtLocation(player, interactedLocation)
                }
            }
        }
    }

    private fun handleCrateAtLocation(source: ServerPlayer, crateLocation: ServerLocation) {
        val crateName =
            plugin.crateDatabase.getCrateAtLocation(crateLocation)!! // Safe due to previous check in listener
        val kitName = plugin.configUtils.getCrateKitMessage(crateName)

        if (plugin.kitUtils.redeemCrateKit(kitName, crateLocation)) {
            try {
                applyEffects(crateLocation)
            } catch (e: CommandException) {
                plugin.messageUtils.notifyError("Error spawning rocket after player opened crate!")
            }

            plugin.crateDatabase.removeCrate(crateLocation)
            crateLocation.removeBlock()
        } else {
            plugin.messageUtils.notifyError("Error redeeming crate to user ${source.name()} with kit $kitName")
        }

        plugin.messageUtils.announceCrateFound(source, crateLocation, crateName)
    }

    private fun applyEffects(location: ServerLocation) {
        val firework: Entity = location.createEntity(EntityTypes.FIREWORK_ROCKET.get())
        firework.offer(Keys.FIREWORK_FLIGHT_MODIFIER, Ticks.of(0))
        firework.offer(Keys.EXPLOSION_RADIUS, 0)
        firework.offer(
            Keys.FIREWORK_EFFECTS, listOf(
                FireworkEffect.builder()
                    .color(Color.BLACK)
                    .shape(FireworkShapes.BURST)
                    .build()
            )
        )
        location.spawnEntity(firework)
    }
}
