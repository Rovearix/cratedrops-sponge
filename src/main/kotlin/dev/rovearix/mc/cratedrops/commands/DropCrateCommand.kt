package dev.rovearix.mc.cratedrops.commands

import dev.rovearix.mc.cratedrops.CrateDrops
import dev.rovearix.mc.cratedrops.data.Crate
import dev.rovearix.mc.cratedrops.utils.CratePermissionUtils
import dev.rovearix.mc.vst.shared.utils.PermissionUtils
import dev.rovearix.mc.vst.shared.utils.RandomUtils
import dev.rovearix.mc.vst.sponge.commands.SpongeVoidCommand
import org.spongepowered.api.block.BlockType
import org.spongepowered.api.block.BlockTypes
import org.spongepowered.api.command.parameter.CommandContext
import org.spongepowered.api.command.parameter.Parameter
import org.spongepowered.api.world.server.ServerLocation

class DropCrateCommand(
    private val plugin: CrateDrops
) : SpongeVoidCommand(plugin) {
    override val commandHandle: String = "dropcrate"
    override val description: String = "/$commandHandle [amount] [crate name] Drops crates randomly " +
            "in the wild, default amount: 1, default crate: randomly selected."
    override val permissionNode: PermissionUtils.Permission = CratePermissionUtils.DROP_CRATE_PERMISSION

    override val parameters: List<Parameter> = listOf(AMOUNT_ARGUMENT, CRATE_ARGUMENT)

    override fun commandBody(context: CommandContext) {
        val quantityOfCrates = getCrateAmount(context)
        val selectedCrate: Crate? = getCrateSelection(context)?.let { crateName ->
            plugin.cache.validCrates.firstOrNull { it.name == crateName }.apply {
                if (this == null) {
                    plugin.messageUtils.notifyError("Crate \"$crateName\" not found in the config!", context)
                    return
                }
            }
        }
        spawnCrates(quantityOfCrates, selectedCrate)
    }

    private fun spawnCrates(quantity: Int, crate: Crate?) {
        (0 until quantity).forEach {
            val selectedCrate = crate ?: selectRandomCrate()
            val dropLocation = selectDroppableLocation()
            dropLocation.setBlockType(CRATE_BLOCK_TYPE)
            plugin.crateDatabase.addCrate(dropLocation, selectedCrate)
            plugin.messageUtils.announceCrate(dropLocation, selectedCrate)
        }
    }

    private fun selectRandomCrate(): Crate {
        val randomCrateIndex = RandomUtils.getRandomSelection(plugin.cache.validCrates, plugin.cache.chanceTotal)
        return plugin.cache.validCrates[randomCrateIndex]
    }

    private fun selectDroppableLocation(): ServerLocation {
        var x = 0
        var z = 0
        var y = -1
        var numTries = 20
        while (y == -1 && numTries > 0) {
            val dropZone = plugin.cache.dropRegion.randomZone
            val dropPoint = dropZone.randomPoint
            x = dropPoint.getX().toInt()
            z = dropPoint.getY().toInt()
            y = findValidDropY(x, z)
            if (y == -1) {

                plugin.messageUtils.notifyError("Warning: Could not find safe drop location at X: $x Z: $z")
            }
            numTries--
        }
        return plugin.cache.targetWorld.location(x, y, z)
    }

    private fun findValidDropY(x: Int, z: Int): Int {
        val buildLimit = 255
        if (blockIsAir(x, buildLimit, z)) {
            for (y in buildLimit - 1 downTo 1) {
                if (!blockIsAir(x, y, z)) {
                    return y + 1
                }
            }
        }
        return -1
    }

    private fun blockIsAir(x: Int, y: Int, z: Int): Boolean {
        return plugin.cache.targetWorld.location(x, y, z).blockType() == BlockTypes.AIR.get()
    }

    private fun getCrateAmount(context: CommandContext): Int {
        return context.one(AMOUNT_ARGUMENT).orElse(1)
    }

    private fun getCrateSelection(context: CommandContext): String? {
        return context.one(CRATE_ARGUMENT).orElse(null)
    }

    companion object {
        private val AMOUNT_ARGUMENT: Parameter.Value<Int> = Parameter.integerNumber().optional().key("amount").build()

        //TODO: Add auto tab completion for possible results
        private val CRATE_ARGUMENT: Parameter.Value<String> = Parameter.string().optional().key("crate").build()

        //TODO: Add configuration option for this type; move to config setting
        val CRATE_BLOCK_TYPE: BlockType = BlockTypes.TRAPPED_CHEST.get()
    }
}