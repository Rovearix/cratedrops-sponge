package dev.rovearix.mc.cratedrops.commands

import dev.rovearix.mc.cratedrops.CrateDrops
import dev.rovearix.mc.cratedrops.utils.CratePermissionUtils
import dev.rovearix.mc.vst.shared.utils.PermissionUtils
import dev.rovearix.mc.vst.sponge.commands.SpongeVoidCommand
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.NamedTextColor
import org.spongepowered.api.command.parameter.CommandContext

class ClearActiveCratesCommand(
    private val plugin: CrateDrops
) : SpongeVoidCommand(plugin) {
    override val commandHandle: String = "clearactivecrates"
    override val description: String = "/$commandHandle expires all active crates."
    override val permissionNode: PermissionUtils.Permission = CratePermissionUtils.CLEAR_ACTIVE_CRATES_PERMISSION

    override fun commandBody(context: CommandContext) {
        val numberOfCrates = plugin.crateDatabase.allCrateLocations.size
        if (numberOfCrates > 0) {
            (0 until numberOfCrates).forEach {
                plugin.crateDatabase.removeOldestCrate()
            }
        } else {
            context.sendMessage(Component.text("No active crates found.").color(NamedTextColor.GREEN))
        }
    }
}
