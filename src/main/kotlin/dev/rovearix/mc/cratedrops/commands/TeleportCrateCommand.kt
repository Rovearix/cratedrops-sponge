package dev.rovearix.mc.cratedrops.commands

import dev.rovearix.mc.cratedrops.CrateDrops
import dev.rovearix.mc.cratedrops.utils.CratePermissionUtils
import dev.rovearix.mc.vst.shared.utils.PermissionUtils
import dev.rovearix.mc.vst.sponge.commands.SpongeVoidCommand
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.NamedTextColor
import org.spongepowered.api.command.parameter.CommandContext
import org.spongepowered.api.command.parameter.Parameter
import org.spongepowered.api.entity.living.player.server.ServerPlayer
import kotlin.jvm.optionals.getOrNull

class TeleportCrateCommand(
    private val plugin: CrateDrops
) : SpongeVoidCommand(plugin) {
    override val commandHandle: String = "tpcrate"
    override val description: String = "/$commandHandle (crate index) [target] Teleports a player  to the specified crate. " +
            "By default this will be the player executing the command"
    override val permissionNode: PermissionUtils.Permission = CratePermissionUtils.TP_CRATE_PERMISSION

    override val parameters: List<Parameter> = listOf(CRATE_INDEX_ARGUMENT, TARGET_ARGUMENT)

    override fun commandBody(context: CommandContext) {
        val crateIndex = getCrateIndex(context)
        val player = getTarget(context) as? ServerPlayer

        if (player == null) {
            context.sendMessage(Component.text("Command requires player as argument when running from console!").color(NamedTextColor.RED))
            return
        }
        if (player != context.cause().root() && !player.hasPermission(CratePermissionUtils.TP_CRATE_OTHERS_PERMISSION.id)) {
            context.sendMessage(Component.text("You are not authorized to run this command on others!").color(NamedTextColor.RED))
            return
        }

        plugin.crateDatabase.allCrateLocations.let { locations ->
            if (locations.isNotEmpty()) {
                if (crateIndex in locations.indices) {
                    player.setLocation(locations[crateIndex].add(0.5, 1.0, 0.5))
                } else {
                    context.sendMessage(Component.text("Please select a valid crate index!").color(NamedTextColor.RED))
                }
            } else {
                context.sendMessage(Component.text("No crates exist for you to teleport to!").color(NamedTextColor.RED))
            }
        }
    }

    private fun getCrateIndex(context: CommandContext): Int {
        return context.one(CRATE_INDEX_ARGUMENT).get()
    }

    private fun getTarget(context: CommandContext): Any {
        return context.one(TARGET_ARGUMENT).getOrNull() ?: context.cause().root()
    }

    companion object {
        //TODO: Add auto tab completion for possible results
        private val CRATE_INDEX_ARGUMENT: Parameter.Value<Int> = Parameter.integerNumber().key("crateIndex").build()

        private val TARGET_ARGUMENT: Parameter.Value<ServerPlayer> = Parameter.playerOrTarget().optional().key("target").build()
    }
}