package dev.rovearix.mc.cratedrops.commands

import dev.rovearix.mc.cratedrops.CrateDrops
import dev.rovearix.mc.cratedrops.utils.CratePermissionUtils
import dev.rovearix.mc.vst.shared.utils.PermissionUtils
import dev.rovearix.mc.vst.sponge.commands.SpongeVoidCommand
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.NamedTextColor
import org.spongepowered.api.command.parameter.CommandContext

class ActiveCratesCommand(
    private val plugin: CrateDrops
) : SpongeVoidCommand(plugin) {
    override val commandHandle: String = "activecrates"
    override val description: String = "/$commandHandle shows a list of all the locations of unclaimed crates in the wild."
    override val permissionNode: PermissionUtils.Permission = CratePermissionUtils.ACTIVE_CRATES_PERMISSION

    override fun commandBody(context: CommandContext) {
        val crateLocations = plugin.crateDatabase.allCrateLocations
        if (crateLocations.isNotEmpty()) {
            context.sendMessage(Component.text("Currently active crates:").color(NamedTextColor.DARK_GREEN))
            crateLocations.forEachIndexed { index, location ->
                val rawMessage = "Crate at: ${location.blockX()} ${location.blockY()} ${location.blockZ()}"
                var message: Component = Component.text(rawMessage)
                if (context.hasPermission(CratePermissionUtils.WARP_TEXT_PERMISSION.id)) {
                    message = plugin.textUtils.generateTeleportMessage(message, index)
                }
                context.sendMessage(message.color(NamedTextColor.GREEN))
            }
        } else {
            context.sendMessage(Component.text("No active crates found.").color(NamedTextColor.GREEN))
        }
    }
}