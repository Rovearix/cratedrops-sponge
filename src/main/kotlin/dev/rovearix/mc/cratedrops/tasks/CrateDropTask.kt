package dev.rovearix.mc.cratedrops.tasks

import dev.rovearix.mc.cratedrops.CrateDrops
import org.spongepowered.api.Sponge
import org.spongepowered.api.command.exception.CommandException

class CrateDropTask(private val plugin: CrateDrops) : Runnable {
    override fun run() {
        try {
            var command = "dropcrate"
            if (!plugin.configUtils.isAutoDropRandomKitEnabled) {
                command += " " + plugin.configUtils.autoDropDefinedKit
            }
            Sponge.server().commandManager().process(Sponge.systemSubject(), command)
        } catch (e: CommandException) {
            plugin.messageUtils.notifyError("Failed to AutoDrop crate!")
        }
        plugin.scheduleUtils.scheduleDrop()
    }
}
