# CrateDrops-Sponge

This plugin is designed for server administrators to spawn in random crates in a configurable world. The crates will
spawn in somewhere inside of that world's border on the surface. You can specify the quantity of crates and what kit
will be inside of it with the command /dropcrate.

Config:

- Announcement when the crate is dropped and picked up.
- World crate will spawn in.
- Kits that will be randomly selected when dropping the crate.

Dependencies:

- Nucleus 