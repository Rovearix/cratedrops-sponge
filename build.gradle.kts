import dev.rovearix.gradle.plugins.MCServerAddonPlugin
import dev.rovearix.gradle.plugins.data.Sponge11ServerConfig
import org.gradle.kotlin.dsl.create
import org.spongepowered.gradle.plugin.config.PluginLoaders
import org.spongepowered.plugin.metadata.model.PluginDependency

plugins {
    kotlin("jvm")
    id("org.spongepowered.gradle.plugin") version "2.2.0"
    id("com.github.johnrengelman.shadow") version "8.1.1"
}

extensions.create<Sponge11ServerConfig>("config")
apply<MCServerAddonPlugin>()

group = "dev.rovearix.mc.cratedrops"
val pluginVersion = "4.0.0"
val apiVersion = "11.0.0"
val fullVersion = "$pluginVersion-API-$apiVersion"
version = pluginVersion

repositories {
    mavenCentral()
}

sponge {
    apiVersion(apiVersion)
    license("GPL")
    loader {
        name(PluginLoaders.JAVA_PLAIN)
        version("1.0")
    }
    plugin("cratedrops") {
        displayName("CrateDrops")
        entrypoint("$group.CrateDrops")
        description("Plugin that spawns in crates at a random locations.")
        links {
            homepage("https://ore.spongepowered.org/rovearix/CrateDrops/")
            source("https://gitlab.com/Rovearix/cratedrops-sponge")
            issues("https://gitlab.com/Rovearix/cratedrops-sponge/-/issues")
        }
        contributor("Rovearix") {
            description("Plugin Developer")
        }
        dependency("spongeapi") {
            loadOrder(PluginDependency.LoadOrder.AFTER)
            optional(false)
        }
    }
}

val javaTarget = 22
java {
    sourceCompatibility = JavaVersion.toVersion(javaTarget)
    targetCompatibility = JavaVersion.toVersion(javaTarget)
    if (JavaVersion.current() < JavaVersion.toVersion(javaTarget)) {
        toolchain.languageVersion.set(JavaLanguageVersion.of(javaTarget))
    }
}

tasks.withType(JavaCompile::class).configureEach {
    options.apply {
        encoding = "utf-8" // Consistent source file encoding
        if (JavaVersion.current().isJava10Compatible) {
            release.set(javaTarget)
        }
    }
}

// Make sure all tasks which produce archives (jar, sources jar, javadoc jar, etc) produce more consistent output
tasks.withType(AbstractArchiveTask::class).configureEach {
    isReproducibleFileOrder = true
    isPreserveFileTimestamps = false
}

dependencies {
    fun includeDependency(dependency: Dependency) {
        api(dependency)
        shadow(dependency)
    }
    api(create("org.spongepowered:spongeapi:$apiVersion"))
    includeDependency(project(":void-sponge:api-11"))
    includeDependency(create(kotlin("stdlib-jdk8")))
    includeDependency(create(kotlin("reflect")))
    includeDependency(create("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.8.1"))
    includeDependency(create("org.jetbrains.kotlinx:kotlinx-serialization-core:1.7.1"))
}

tasks.shadowJar {
    archiveFileName.set("CrateDrops-$fullVersion.jar")
    configurations = listOf(project.configurations.shadow.get())
    //TODO: Remove other meta inf garbage
}

tasks.jar {
    enabled = false
}

tasks.build {
    dependsOn(tasks.shadowJar)
}

artifacts {
    archives(tasks.shadowJar)
}